#!/bin/bash

die() { echo >&2 "$@"; exit 1; }

lines=`git status --porcelain | grep -v ^?? | wc -l`
test "$lines" != "0" && die "stash the git changes"

if test -d config; then
    cd config
    git pull
    cd ..
else
    git clone git://git.savannah.gnu.org/config.git
fi


cp config/config.guess .
cp config/config.sub .

lines=`git status --porcelain config.guess config.sub | wc -l`
if test "$lines" = "0"; then
    echo "no upstream change"
    exit 0
fi

make -C config check || die "make check failed"

rpmdev-bumpspec automake.spec \
        -c "update config.{guess,sub} to gnuconfig git HEAD" \
    || die "can not bump spec"

git add -f config.sub config.guess automake.spec
git commit -m "gnuconfig: sync with upstream git" --no-verify
